/* This document describe how to run NLTK processors */

nlp-----|
	|----pythonServer-------|	
	|			|----server.py ( main file to run python server )
	|----nodeServer-|
			|
			|-------|----server.js ( main file to run node server )

1. Architecture

description: 3 tier architecture

		   (TCP sockets)		
	         request/response		( push data ) 
python server <-------------------> node server---------------> mongodb 
	           first step			 second step		


2. How to run python server ?

   2.1 Install python2.7 and required dependencies [ zerorpc, nltk ], through command line

   2.2 Navigate in to node server directory i.e /nlp/nodeserver

   2.3 command: 

	$ alias python=python2.7
	$ python server.py
   
   2.4 thats it, python server should be up at http://localhost:9602

3. How to run node server ?

   3.1 Install node and dependencies [ ZeroRPC, libevent, mongodb driver, express ]

   3.2 Navigate into node server directory ie. /nlp/nodeserver

   3.3 commands:

	$ node server.js

   3.4 Node server should be running at http://localhost:4000/

4. Service URLs:

   From single value data
	4.1 http://localhost:4000/api?method=tokenize&type=inputSingle&data=Country


   From multiple valued source string
	4.2 http://localhost:4000/api?method=tokenize&type=inputMultiple&data=Mary has pretty lamb

   From Webpage or source document
	4.3 http://localhost:4000/api?method=tokenize&type=src&data=http://192.168.0.134/nlp/data/data.txt&start=6&limit=2000 
           OR
	    http://localhost:4000/api?method=tokenize&type=srcNew&data=http://192.168.0.134/nlp/data/data.txt

Thats it, thanks 
