var mongoose = require('mongoose');

// Subdocument schema for votes
exports.MssSchema = new mongoose.Schema({
	userName: { type: String, required: true },
	firstName: { type: String },
	lastName: { type: String },
	email: { type: String, required: true },
});
